package business;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;
import javax.faces.bean.ManagedBean;

import beans.Order;

@ManagedBean
@Stateless
@Local(OrdersBusinessInterface.class)
@Alternative
public class OrdersBusinessService implements OrdersBusinessInterface {
	
	private List<Order> orders = new ArrayList<Order>();
	
    public OrdersBusinessService() {
		super();
        // TODO Auto-generated constructor stub
    	orders.add(new Order("00000", "This is Product 1", 1.00f, 1));
		orders.add(new Order("00001", "This is Product 2", (float)2.00, 2));
		orders.add(new Order("00002", "This is Product 3", (float)3.00, 3));
		orders.add(new Order("00003", "This is Product 4", (float)4.00, 4));
		orders.add(new Order("00004", "This is Product 5", (float)5.00, 5));
		orders.add(new Order("00005", "This is Product 6", (float)6.00, 6));
		orders.add(new Order("00006", "This is Product 7", (float)7.00, 7));
		orders.add(new Order("00007", "This is Product 8", (float)8.00, 8));
		orders.add(new Order("00008", "This is Product 9", (float)9.00, 9));
		orders.add(new Order("00009", "This is Product 10", (float)10.00, 10));
    }


    public void test() {
        // TODO Auto-generated method stub
    	System.out.println("Hello from OrdersBusinessService.test");
    }
    
    public List<Order> getOrders() {
    	return orders;
    }
    
    public void setOrders(List<Order> orders) {
    	this.orders = orders;
    }

}
